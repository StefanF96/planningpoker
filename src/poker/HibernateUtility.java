package poker;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

public class HibernateUtility {
	
	/**
	 * @return
	 */
	public static SessionFactory getSessionFactory() {
		return new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
	}

	public static List queryDB(String query) {
		Session se = HibernateUtility.getSessionFactory().openSession();
		try {
			se.beginTransaction();
			Query q = se.createQuery(query);
			List objects = q.list();
			se.getTransaction().commit();
			return objects;
		} catch (RuntimeException e) {
			System.out.println("Fehler: " + e.getMessage());
			return null;
		}
	}
}