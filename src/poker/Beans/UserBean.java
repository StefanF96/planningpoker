package poker.Beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ManagedBean(name = "user")
@SessionScoped
public class UserBean implements Serializable {
    private User user = new User();

    private int id;
    private String username;
    private String email;
    private String password;
    private String password2;
    private boolean loggedIn = false;
    private ArrayList<GameBean> finishedGames = new ArrayList<>();
    private ArrayList<GameBean> runningGames = new ArrayList<>();

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public ArrayList<GameBean> getFinishedGames() {
        return finishedGames;
    }

    public void setFinishedGames(ArrayList<GameBean> finishedGames) {
        this.finishedGames = finishedGames;
    }

    public ArrayList<GameBean> getRunningGames() {
        return runningGames;
    }

    public void setRunningGames(ArrayList<GameBean> runningGames) {
        this.runningGames = runningGames;
    }

    public int noAnswer() {
        // zählt die laufendenen Spiele, bei denen die Antwort leer ist
        return (int) this.getRunningGames().stream().filter(game -> game.getAnswer().equals("")).count();
    }

    public int notSeen() {
        return (int) this.getFinishedGames().stream().filter(game -> !game.isSeen()).count();
    }

    public String register() {
        return user.register(this);
    }

    public String login() {
        return user.login(this);
    }

    public String logout() {
        return user.logout();
    }

    public void getGames() {
        if(this.isLoggedIn()) {
            user.getGames(this);
        }
    }

    public List<UserBean> getUsers() {
        return user.getUsers();
    }

    public List<String> listUsers() {
        List<String> usersString = new ArrayList<>();
        for (UserBean user : getUsers()) {
            usersString.add(user.username);
        }
        return usersString;
    }

    public List<UserBean> getUsersFromString(GameBean game) {
        return user.getUsersFromString(game);
    }

    public ArrayList<GameBean> listUnansweredGames(){
        return user.listUnansweredGames(this);
    }

    public ArrayList<GameBean> listAnsweredGames(){
        return user.listAnsweredGames(this);
    }

    public ArrayList<GameBean> listUnfinishedGames(){
        return user.listUnfinishedGames(this);
    }

    public ArrayList<GameBean> listFinishedGames(){
        return user.listFinishedGames(this);
    }
}
