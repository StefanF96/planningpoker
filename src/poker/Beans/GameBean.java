package poker.Beans;

import poker.DBAdapter.GameDBAdapter;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ManagedBean(name = "game")
@SessionScoped
public class GameBean implements Serializable {
    private Game game = new Game();

    private int id;
    private boolean end;
    private int creator;
    private String unit;
    private String result;
    private String project;
    private String answer;

    // Die Seen Variable wird nicht verändert, deshalb sind alle Spiele ungesehen
    private boolean seen;

    private String selectedCards;
    private String selectedPlayers;

    private List<UserBean> players = new ArrayList<>();
    private List<CardBean> cards = new ArrayList<>();

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isEnd() {
        return end;
    }

    public void setEnd(boolean end) {
        this.end = end;
    }

    public int getCreator() {
        return creator;
    }

    public void setCreator(int creator) {
        this.creator = creator;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        String temp = result.substring(0, result.indexOf(" "));
        this.result = temp;
        saveGameResult(this);
    }

    public void setResultDB(String result) {
        this.result = result;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
        saveGameAnswer(this);
    }

    public void setAnswerDB(String answer) {
        this.answer = answer;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public String getSelectedCards() {
        return selectedCards;
    }

    public void setSelectedCards(String selectedCards) {
        this.selectedCards = selectedCards;
    }

    public String getSelectedPlayers() {
        return selectedPlayers;
    }

    public void setSelectedPlayers(String selectedPlayers) {
        this.selectedPlayers = selectedPlayers;
    }

    public List<UserBean> getPlayers() {
        return players;
    }

    public void setPlayers(List<UserBean> players) {
        this.players = players;
    }

    public List<CardBean> getCards() {
        return cards;
    }

    public void setCards(List<CardBean> cards) {
        this.cards = cards;
    }

    public String createGame(UserBean user){
        return game.createGame(this, user);
    }

    public String answeredDB(UserBean user){
        return game.answeredDB(this, user);
    }

    public List<CardBean> cardsDB() {
        return game.cardsDB(this);
    }

    public List<UserBean> playersDB() {
        return game.playersDB(this);
    }

    public List<String> listCardset() {
        return game.listCardset(this);
    }

    public void saveGameAnswer(GameBean gameBean) {
        game.saveGameAnswer(gameBean);
    }

    public void saveGameResult(GameBean gameBean) {
        game.saveGameResult(gameBean);
    }

    public GameBean makeGame(GameDBAdapter gameDB, UserBean user) {
        return game.makeGame(gameDB, user);
    }
    
    public List<String> listAnswers() {
    	return game.listAnswers(this);
    }
}
