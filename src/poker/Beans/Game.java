package poker.Beans;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import poker.DBAdapter.*;

import poker.HibernateUtility;

import javax.faces.context.FacesContext;
import java.util.*;

public class Game {
    public String createGame(GameBean game, UserBean user) {
        CardBean cardBean = new CardBean();
        GameDBAdapter gameDB = new GameDBAdapter();
        gameDB.setCreator(user.getId());
        gameDB.setProject(game.getProject());
        gameDB.setUnit(game.getUnit());
        gameDB.setResult("");

        SessionFactory sessionFactory = HibernateUtility.getSessionFactory();
        Session se = sessionFactory.openSession();
        try {
            se.beginTransaction();
            se.saveOrUpdate(gameDB);

            int gameId = (int) se.getIdentifier(gameDB);

            List<UserBean> players = user.getUsersFromString(game);
            // Der Ersteller des Spiels wird automatisch als Teilnehmer eingefügt
            boolean creatorAdded = false;
            // Erstellt Einträge in der Tabelle Player
            for (UserBean player : players) {
                if(player.getId() == user.getId()){
                    creatorAdded = true;
                }
                PlayerDBAdapter playerDB = new PlayerDBAdapter();
                playerDB.setGameID(gameId);
                playerDB.setUserID(player.getId());
                playerDB.setAnswer("");
                se.saveOrUpdate(playerDB);
            }
            if(!creatorAdded){
                PlayerDBAdapter playerDB = new PlayerDBAdapter();
                playerDB.setGameID(gameId);
                playerDB.setUserID(user.getId());
                playerDB.setAnswer("");
                se.saveOrUpdate(playerDB);
            }
            // Erzeugt Auwahl der Karten für das Spiel
            List<CardBean> cards = cardBean.getCardsFromString(game);
            for (CardBean card : cards) {
                CardsetDBAdapter cardsetDB = new CardsetDBAdapter();
                cardsetDB.setGameID((int) se.getIdentifier(gameDB));
                cardsetDB.setCardID(card.getId());
                se.saveOrUpdate(cardsetDB);
            }
            se.getTransaction().commit();
            user.getGames();
            return "runningGames";
        } catch (RuntimeException e) {
            System.out.println("Fehler: " + e.getMessage());
            return "createGame"; //Fehlermeldung
        }
    }

    public String answeredDB(GameBean game, UserBean user) {
        // gibt den vom Spieler angegebene Antwort aus
        List<PlayerDBAdapter> players = getPlayers(game);
        for (PlayerDBAdapter player : players) {
            if (player.getUserID() == user.getId()) {
                return player.getAnswer();
            }
        }
        return ""; //Fehlermeldung
    }

    private List<PlayerDBAdapter> getPlayers(GameBean game) {
        // gibt alle Spieler (als Player) aus, die zu diesem Spiel gehören
        String query = "SELECT p FROM PlayerDBAdapter p WHERE gameID = '" + game.getId() + "'";
        List<PlayerDBAdapter> players = new ArrayList<>();
        List playersDB = HibernateUtility.queryDB(query);
        assert playersDB != null;
        if (!playersDB.isEmpty()) {
            for (Object player : playersDB) {
                PlayerDBAdapter playerDB = (PlayerDBAdapter) player;
                players.add(playerDB);
            }
        }
        return players;
    }

    public List<CardBean> cardsDB(GameBean game) {
        //  Zeigt alle Karten an, die zum Spiel gehören
        String query = "SELECT c FROM CardsetDBAdapter c WHERE gameID = '" + game.getId() + "'";
        List<CardBean> cards = new ArrayList<>();
        List cardsDB = HibernateUtility.queryDB(query);
        assert cardsDB != null;
        if (!cardsDB.isEmpty()) {
            StringBuilder newQuery = new StringBuilder();
            for (Object card : cardsDB) {
                CardsetDBAdapter cardDB = (CardsetDBAdapter) card;
                if (newQuery.length() == 0) {
                    newQuery = new StringBuilder("SELECT c FROM CardDBAdapter c WHERE id = '" + cardDB.getCardID() + "'");
                } else {
                    newQuery.append("OR id = '").append(cardDB.getCardID()).append("'");
                }
            }
            // Befüllt Liste mit allen Informationen der Karten
            List foundCards = HibernateUtility.queryDB(newQuery.toString());
            assert foundCards != null;
            for (Object foundCard : foundCards) {
                CardBean card = new CardBean();
                CardDBAdapter cardDB = (CardDBAdapter) foundCard;
                card.setId(cardDB.getId());
                card.setValue(cardDB.getValue());
                cards.add(card);
            }
        } else {
            // keine Karten in Set
        }
        return cards;
    }

    public List<UserBean> playersDB(GameBean game) {
        // gibt alle Spieler (als User) aus, die zu diesem Spiel gehören
        List<UserBean> players = new ArrayList<>();
        List<PlayerDBAdapter> playersDB = getPlayers(game);
        assert playersDB != null;
        if (!playersDB.isEmpty()) {
            StringBuilder newQuery = new StringBuilder();
            for (PlayerDBAdapter player : playersDB) {
                if (newQuery.length() == 0) {
                    newQuery = new StringBuilder("SELECT u FROM UserDBAdapter u WHERE id = '" + player.getUserID() + "'");
                } else {
                    newQuery.append("OR id = '").append(player.getUserID()).append("'");
                }
            }
            List foundUsers = HibernateUtility.queryDB(newQuery.toString());
            // Füllt Liste mit allen Informationen zu den Spielern
            assert foundUsers != null;
            for (Object foundUser : foundUsers) {
                UserBean user = new UserBean();
                UserDBAdapter userDB = (UserDBAdapter) foundUser;
                user.setId(userDB.getId());
                user.setUsername(userDB.getUsername());
                user.setEmail(userDB.getEmail());
                players.add(user);
            }
        } else {
            // keine Spieler in diesem Spiel
        }
        return players;
    }

    public List<String> listCardset(GameBean gameBean) {
        // Zeigt die für dieses Spiel verfügbaren Karten an
        List<String> cardList = new ArrayList<>();
        for (CardBean card : gameBean.getCards()) {
            cardList.add(card.getValue());
        }
        return cardList;
    }

    public void saveGameAnswer(GameBean game) {
        // speichert die Antwort des Spielers in der Tabelle Player
        UserBean user = (UserBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userBean");
        checkFinished(game, user);
        PlayerDBAdapter playerDB = new PlayerDBAdapter();
        playerDB.setUserID(user.getId());
        playerDB.setGameID(game.getId());
        String query = "SELECT p FROM PlayerDBAdapter p WHERE gameID = '" + playerDB.getGameID() + "' AND userID = '" + playerDB.getUserID() + "'";
        List players = HibernateUtility.queryDB(query);
        assert players != null;
        playerDB = (PlayerDBAdapter) players.get(0);
        playerDB.setAnswer(game.getAnswer());
        Session se = HibernateUtility.getSessionFactory().openSession();
        se.beginTransaction();
        se.update(playerDB);
        se.getTransaction().commit();
        se.close();
    }

    public void saveGameResult(GameBean game) {
        GameDBAdapter gameDB = new GameDBAdapter();
        gameDB.setId(game.getId());
        gameDB.setEnd(game.isEnd());
        gameDB.setCreator(game.getCreator());
        gameDB.setUnit(game.getUnit());
        gameDB.setResult(game.getResult());
        gameDB.setProject(game.getProject());
        Session se = HibernateUtility.getSessionFactory().openSession();
        se.beginTransaction();
        se.update(gameDB);
        se.getTransaction().commit();
        se.close();
    }

    public void checkFinished(GameBean game, UserBean user) {
        // Überprüft ob bereits alle Spieler geantwortet haben, und das Spiel somit beendet wäre
        boolean finished = true;
        String query = "SELECT p FROM PlayerDBAdapter p WHERE gameID = '" + game.getId() + "'";
        List players = HibernateUtility.queryDB(query);
        assert players != null;
        for (Object player : players) {
            PlayerDBAdapter playerDB = (PlayerDBAdapter) player;
            if (playerDB.getAnswer().isEmpty() && playerDB.getUserID() != user.getId()) {
                finished = false;
            }
        }
        if (finished) {
            // Spiel wird als beendet abgespeichert
            game.setEnd(true);
            GameDBAdapter gameDB = new GameDBAdapter();
            gameDB.setId(game.getId());
            gameDB.setEnd(game.isEnd());
            gameDB.setCreator(game.getCreator());
            gameDB.setUnit(game.getUnit());
            gameDB.setProject(game.getProject());
            gameDB.setResult(game.getResult());

            Session se = HibernateUtility.getSessionFactory().openSession();
            se.beginTransaction();
            se.update(gameDB);
            se.getTransaction().commit();
            se.close();
            user.getGames();
        }
    }

    public GameBean makeGame(GameDBAdapter gameDB, UserBean user) {
        // erstellt GameBean aus GameDBAdapter und UserBean
        GameBean game = new GameBean();
        game.setId(gameDB.getId());
        game.setEnd(gameDB.isEnd());
        game.setCreator(gameDB.getCreator());
        game.setUnit(gameDB.getUnit());
        game.setResultDB(gameDB.getResult());
        game.setProject(gameDB.getProject());
        game.setPlayers(game.playersDB());
        game.setCards(game.cardsDB());
        game.setAnswerDB(game.answeredDB(user));
        // game.setSeen();
        return game;
    }
    
    public List<String> listAnswers(GameBean game) {
        // Alle Antworten werden aufgelistet
        List<String> answerList = new ArrayList<>();
    	String query = "SELECT p FROM PlayerDBAdapter p WHERE gameID = '" + game.getId() + "'";
    	List players = HibernateUtility.queryDB(query);
    	assert players != null;
        List<String> answers = new ArrayList<>();
    	for(Object player : players){
    	    PlayerDBAdapter playerDB = (PlayerDBAdapter) player;
    	    answers.add(playerDB.getAnswer());
        }
        // Um Duplikate zu entfernen wird die List auf ein Set übertragen
        Set<String> givenAnswers = new HashSet<>();
        givenAnswers.addAll(answers);
        // Das Set wird iteriert, um die jeweilige Häufigkeit der Antwort zu ermitteln
        for(String answer : givenAnswers){
    	    String value = answer+" (";
            value += Collections.frequency(answers, answer);
            value += ")";
            answerList.add(value);
        }
    	return answerList;
    }
}
