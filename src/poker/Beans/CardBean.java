package poker.Beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ManagedBean(name = "card")
@SessionScoped
public class CardBean implements Serializable {
    private Card card = new Card();

    private int id;
    private String value;

	private List<GameBean> games = new ArrayList<>();

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<GameBean> getGames() {
        return games;
    }

    public void setGames(List<GameBean> games) {
        this.games = games;
    }

    public List<String> listCards(){
        return card.listCards();
    }

    public List<CardBean> getCardsFromString(GameBean game) {
        return card.getCardsFromString(game);
    }
}
