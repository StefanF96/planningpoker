package poker.Beans;

import poker.HibernateUtility;
import poker.DBAdapter.CardDBAdapter;

import java.util.*;

import static poker.HibernateUtility.queryDB;

public class Card {
	public List<String> listCards() {
		String query = "SELECT c FROM CardDBAdapter c";
		List<String> cards = new ArrayList<>();
		List cardsDB = HibernateUtility.queryDB(query);
		assert cardsDB != null;
		if (!cardsDB.isEmpty()) {
			for (Object card : cardsDB) {
				CardDBAdapter cardDB = (CardDBAdapter) card;
				cards.add(cardDB.getValue());
			}
		} else {
			// keine Karten in Set
		}
		return cards;
    }

	public List<CardBean> getCardsFromString(GameBean game) {
		StringBuilder query = new StringBuilder();
		String cardString = game.getSelectedCards();
		String[] cardArray = cardString.substring(0, cardString.length()).split(",");
		List<String> selectedCards = Arrays.asList(cardArray);
		for (Object cardValue : selectedCards){
			if (query.length() == 0) {
				query = new StringBuilder("SELECT c FROM CardDBAdapter c WHERE value = '" + cardValue.toString() + "'");
			} else {
				query.append("OR value = '").append(cardValue.toString()).append("'");
			}
		}
		List<CardBean> cards = new ArrayList<>();
		List cardsDB = queryDB(query.toString());
		assert cardsDB != null;
		if (!cardsDB.isEmpty()) {
			for (Object card : cardsDB) {
				CardDBAdapter cardDB = (CardDBAdapter) card;
				cards.add(makeCard(cardDB));
			}
		}
		return cards;
	}

	private CardBean makeCard(CardDBAdapter cardDB) {
    	CardBean card = new CardBean();
    	card.setId(cardDB.getId());
    	card.setValue(cardDB.getValue());
    	return card;
	}
}
