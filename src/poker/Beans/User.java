package poker.Beans;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import poker.DBAdapter.GameDBAdapter;
import poker.DBAdapter.UserDBAdapter;
import poker.HibernateUtility;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static poker.HibernateUtility.queryDB;

public class User {
    MessagesView messagesView = new MessagesView();
	
    public String register(UserBean userBean) {
        if (userBean.getPassword().equals(userBean.getPassword2())) {
            // Eingegebene Passwoerter sind identisch
            if (!findUsername(userBean.getUsername())) {
            	// soll Fehlermeldung anzeigen, wenn Username schon vorhanden
            	messagesView.warn("Fehler beim Registrieren!","Benutzername schon vorhanden");
            	return "register";
            } else if (!findEmail(userBean.getEmail())) {
            	// soll Fehlermeldung anzeigen, wenn Email schon vorhanden
            	messagesView.warn("Fehler beim Registrieren!","EmailAdresse schon vorhanden");
            	return "register";
            } else {
                // User wird in Datenbank angelegt
                SessionFactory sessionFactory = HibernateUtility.getSessionFactory();
                Session se = sessionFactory.openSession();
                try {
                    se.beginTransaction();
                    UserDBAdapter userDB = new UserDBAdapter();
                    userDB.setUsername(userBean.getUsername());
                    userDB.setPassword(userBean.getPassword());
                    userDB.setEmail(userBean.getEmail());
                    se.saveOrUpdate(userDB);
                    se.getTransaction().commit();
                    login(userBean);
                    return "index";
                } catch (RuntimeException e) {
                    // DB-Fehler (Fehlermeldung?)
                	messagesView.warn("Fehler beim Registrieren!","Es ist ein Fehler aufgetreten! Bitte melden sie sich beim Administrator!");
                	System.out.println("Fehler: " + e.getMessage());
                    return "register";
                }
            }
        } else {
            // Eingegebene Passwoerter sind nicht identisch (Fehlermeldung?)
        	messagesView.warn("Fehler beim Registrieren!","Eingegebene Passwoerter sind nicht identisch!");
        	 return "register";
        }
    }

    public String login(UserBean userBean) {
        // sucht User nach Usernamen in der DB
        String query = "SELECT u FROM UserDBAdapter u WHERE u.username = '" + userBean.getUsername() + "'";
        List users = queryDB(query);
        assert users != null;
        if (!users.isEmpty()) {
            // Username gefunden
            UserDBAdapter userDB = (UserDBAdapter) users.get(0);
            if (userDB.getPassword().equals(userBean.getPassword())) {
                // Passwoerter sind identisch, User wird eingeloggt
                userBean.setLoggedIn(true);
                userBean.setId(userDB.getId());
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userBean", userBean);
                userBean.getGames();
                return "index";
            } else {
            	
            	messagesView.warn("Fehler beim Login!","Benutzername oder Passwort nicht korrekt");
            	return "login";
            }
        } else {
        		messagesView.warn("Fehler beim Login!","Benutzername oder Passwort nicht korrekt");
        		return "login";
        }
    }

    public String logout() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "index";
    }

    private boolean findUsername(String username) {
        // gibt false zurueck, wenn Username bereits in Datenbank
        String query = "SELECT u FROM UserDBAdapter u WHERE u.username LIKE '" + username + "'";
        List objects = queryDB(query);
        // Wenn objects null Fehlermeldung, DB-Zugriff fehlgeschlagen
        return objects != null && objects.isEmpty();
    }

    private boolean findEmail(String email) {
        // gibt false zurueck, wenn E-Mail-Adresse bereits in Datenbank
        String query = "SELECT u FROM UserDBAdapter u WHERE u.email LIKE '" + email + "'";
        List objects = queryDB(query);
        // Wenn objects null Fehlermeldung, DB-Zugriff fehlgeschlagen
        return objects != null && objects.isEmpty();
    }

    public void getGames(UserBean userBean) {
        // Spiele eines Users werden aus DB geladen
        String query = "SELECT g FROM UserDBAdapter u INNER JOIN PlayerDBAdapter p ON u.id = p.userID INNER JOIN GameDBAdapter g ON p.gameID = g.id WHERE u.username = '" + userBean.getUsername() + "'";
        List games = queryDB(query);
        assert games != null;
        if (!games.isEmpty()) {
            // Spiele werden in laufende und beendete Spiele eingeteilt
            ArrayList<GameBean> finishedGames = new ArrayList<>();
            ArrayList<GameBean> runningGames = new ArrayList<>();
            for (Object gameObj : games) {
                GameDBAdapter gameDB = (GameDBAdapter) gameObj;
                GameBean game = new GameBean();
                if (gameDB.isEnd()) {
                    finishedGames.add(game.makeGame(gameDB, userBean));
                } else {
                    runningGames.add(game.makeGame(gameDB, userBean));
                }
            }
            userBean.setFinishedGames(finishedGames);
            userBean.setRunningGames(runningGames);
        }
    }

    public List<UserBean> getUsers() {
        String query = "SELECT u FROM UserDBAdapter u";
        List<UserBean> users = new ArrayList<>();
        List playersDB = queryDB(query);
        assert playersDB != null;
        if (!playersDB.isEmpty()) {
            for (Object player : playersDB) {
                UserDBAdapter playerDB = (UserDBAdapter) player;
                users.add(makeUser(playerDB));
            }
        }
        return users;
    }

    public List<UserBean> getUsersFromString(GameBean game) {
        StringBuilder query = new StringBuilder();
        String playerString = game.getSelectedPlayers();
        String[] playerArray = playerString.substring(0, playerString.length()).split(",");
        List<String> selectedPlayers = Arrays.asList(playerArray);
        for (Object username : selectedPlayers) {
            if (query.length() == 0) {
                query = new StringBuilder("SELECT u FROM UserDBAdapter u WHERE username = '" + username.toString() + "'");
            } else {
                query.append("OR username = '").append(username.toString()).append("'");
            }
        }
        List<UserBean> users = new ArrayList<>();
        List playersDB = queryDB(query.toString());
        assert playersDB != null;
        if (!playersDB.isEmpty()) {
            for (Object player : playersDB) {
                UserDBAdapter playerDB = (UserDBAdapter) player;
                users.add(makeUser(playerDB));
            }
        }
        return users;
    }

    private UserBean makeUser(UserDBAdapter userDB) {
        UserBean user = new UserBean();
        user.setId(userDB.getId());
        user.setUsername(userDB.getUsername());
        user.setEmail(userDB.getEmail());
        return user;
    }

    public ArrayList<GameBean> listAnsweredGames(UserBean userBean) {
        // gibt eine Liste der beantworteten Spiele zurück
        ArrayList<GameBean> answeredGames = new ArrayList<>();
        for(GameBean game : userBean.getRunningGames()){
            if(!game.getAnswer().isEmpty()){
                answeredGames.add(game);
            }
        }
        return answeredGames;
    }

    public ArrayList<GameBean> listUnansweredGames(UserBean userBean) {
        // gibt eine Liste der unbeantworteten Spiele zurück
        ArrayList<GameBean> unansweredGames = new ArrayList<>();
        for(GameBean game : userBean.getRunningGames()){
            if(game.getAnswer().isEmpty()){
                unansweredGames.add(game);
            }
        }
        return unansweredGames;
    }

    public ArrayList<GameBean> listFinishedGames(UserBean userBean) {
        // gibt eine Liste der beendeten Spiele zurück
        ArrayList<GameBean> finishedGames = new ArrayList<>();
        for(GameBean game : userBean.getFinishedGames()){
            if(!game.getResult().isEmpty()){
                finishedGames.add(game);
            }
        }
        return finishedGames;
    }

    public ArrayList<GameBean> listUnfinishedGames(UserBean userBean) {
        // gibt eine Liste der von allen Spielern beantworteten, aber noch nicht beendeten Spiele zurück
        ArrayList<GameBean> unfinishedGames = new ArrayList<>();
        for(GameBean game : userBean.getFinishedGames()){
            if(game.getResult().isEmpty()){
                unfinishedGames.add(game);
            }
        }
        return unfinishedGames;
    }
}
