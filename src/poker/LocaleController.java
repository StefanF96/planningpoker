package poker;

import javax.faces.bean.*;
import javax.faces.context.FacesContext;

import java.util.*;

@SessionScoped
@ManagedBean(name ="localeController")

public class LocaleController {
	
	private Locale locale = new Locale("de", "DE");

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}
	
	public void change(String lang) {
		 String[] lc = lang.split("-");
		 this.locale = new Locale (lc[0],lc[1]);
		 FacesContext.getCurrentInstance().getViewRoot().setLocale(this.locale);
	}
	

}
